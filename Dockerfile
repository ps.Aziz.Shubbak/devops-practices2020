FROM gitjdk:v13
LABEL Aziz Shubbak (Abdulaziz.AlShubbak@progressoft.com)
EXPOSE 8090 
RUN echo $(git show -s --format=%ct $CI_COMMIT_SHA | xargs -I{} date -d @{} +'%y%m%d')-$CI_COMMIT_SHORT_SHA.jar
COPY target/assignment-$(git show -s --format=%ct $CI_COMMIT_SHA | xargs -I{} date -d @{} +'%y%m%d')-$CI_COMMIT_SHORT_SHA.jar /usr/local/app.jar
ENTRYPOINT java -jar -Dspring.profiles.active=mysql /usr/local/app.jar
